const path = require('path');
const rootPath = path.normalize(__dirname + '/..');
const env = process.env.NODE_ENV || 'development';

const config = {
  development: {
    root: rootPath,
    app: {
      name: 'keroto'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/keroto-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'keroto'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/keroto-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'keroto'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/keroto-production'
  }
};

module.exports = config[env];
