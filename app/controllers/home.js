const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Article = mongoose.model('Article');

module.exports = (app) => {
  app.use('/', router);
};

router.get('/', (req, res, next) => {
  Article.find((err, articles) => {
    if (err) return next(err);
    res.render('index', {
      title: 'Kroto Sumber Rejeki',
      articles: articles
    });
  });
});

router.get('/PerluatauTidakKrotountukBurungKicau', function (req, res, next) {
  var articles = [new Article(), new Article()];
    res.render('gallery1', {
      title: 'Perlu atau Tidak Kroto untuk Burung Kicau - Kroto',
      layout: 'main'
      // scripts: '<script type="text/javascript" src="/js/login.js"></script>'
    });
});

router.get('/CaraMembuatUmpanMancingDariKroto', function (req, res, next) {
  var articles = [new Article(), new Article()];
    res.render('gallery2', {
      title: 'Cara Membuat Umpan Mancing Dari Kroto - Kroto',
      layout: 'main'
      // scripts: '<script type="text/javascript" src="/js/login.js"></script>'
    });
});

router.get('/SoupKrotoBernilaiGiziTinggi', function (req, res, next) {
  var articles = [new Article(), new Article()];
    res.render('gallery3', {
      title: 'Soup Kroto Bernilai Gizi Tinggi',
      layout: 'main'
      // scripts: '<script type="text/javascript" src="/js/login.js"></script>'
    });
});

router.get('/KrotoUntukBahanKosmetikdanObatHerbal', function (req, res, next) {
  var articles = [new Article(), new Article()];
    res.render('gallery4', {
      title: 'Kroto Untuk Bahan Kosmetik dan Obat Herbal - Kroto',
      layout: 'main'
      // scripts: '<script type="text/javascript" src="/js/login.js"></script>'
    });
});

router.get('/contactus', function (req, res, next) {
  var articles = [new Article(), new Article()];
    res.render('contactus', {
      title: 'Contact Us - Kroto',
      layout: 'main'
      // scripts: '<script type="text/javascript" src="/js/login.js"></script>'
    });
});

router.get('/contactus', function (req, res, next) {
  var articles = [new Article(), new Article()];
    res.render('contactus', {
      title: 'Contact Us - Kroto',
      layout: 'main'
      // scripts: '<script type="text/javascript" src="/js/login.js"></script>'
    });
});